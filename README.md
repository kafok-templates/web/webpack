## Introducción
Este es un proyecto Webpack configurado para Typescript, Sass y Electron. Está todo configurado para desarrollo y puesta en producción. Pueden hacerse modificaciones para adaptarse a cada proyecto, pero siempre hay que cambiar el ```name``` del ```package.json```.

Para empezar a usar este repositorio despues de clonarlo usar el siguiente comando para instalar las dependencias en local:

```
$ npm run configure
```

## Caracteristicas
* Superset de Typescript y Sass.
* Configurado para Electron.
* Modo de construcción para producción y desarrollo.
* Ficheros dependientes del entorno.
* Minificado en producción.
* CLI propia para tareas comunes como compilación.
* Linter para Typescript.

### Estructura del proyecto
* **@types**: Aquí irán archivos de definición de typescript personalizados (acabados en ```.d.ts```). Con el comando ```node cli install``` podrán empezar a usarse.
* **build**: Aquí todo lo necesario para construir para producción. El archivo ```main.js``` es el *entrypoint* de Electron en producción.
* **src**: Aquí irá el código de la aplicación.
	* **index.ts**: Es el *entrypoint* de la aplicación, aquí deben importarse el archivo de estilos y cualquier dependencia que quiera usarse.
	* **styles.scss**: Es el archivo principal de estilos, el cual debe ser importado en el ```index.ts```. Otros archivos de estilos pueden ser importados en otros ```.ts``` siempre y cuando estos acaben siendo importados por ```index.ts```.
	* **index.html**: Plantilla HTML donde se inyectarán las fuentes al construir.
	* **assets**: Esta carpeta contendrá todos los archivos que deban ser referenciados, como por ejemplo las imagenes. Esta carpeta se copiará al resultado de la compilación.
* **main.js**: Este es el *entrypoint* de Electron en desarrollo.

### Entornos y plataformas
Los archivos pueden acabar como ```.<ext>``` o como ```<plataforma><entorno>.<ext>```, donde el entorno y plataforma se pasarán por parámetro al construir con Webpack. Al tener varios archivos con el mismo nombre y entornos y/o plataformas diferentes se cogerá el entorno y plataforma para el cual se construye. Siempre debe de haber **un archivo sin entorno** ni plataforma para asegurar que siempre se llama a uno. Y en los **imports** nunca deben ponerse la extensión para que todo funcione correctamente.

Esto es útil cuando un archivo es diferente según si estamos en desarrollo, integración o producción, esto sería el entorno. Por ejemplo, que en desarrollo tuviéramos estilos distintos para recalcar cosas pero en otros entornos debiera verse la versión definitiva de la aplicación.

Por otro lado, la plataforma puede definir para que target queremos compilar, como por ejemplo web, android o desktop, pudiendo hacer una aplicación universal que con código específico por plataforma.

### CLI Webpack
Para ejecutar Webpack se requieren los siguientes paramatros:
* **mode**: Si se construye para producción o desarrollo. A no ser que se esté compilando para producción, dejar en desarrollo. Por defecto ```development```.
* **environment**: Compila la versión de cada fichero en el entorno indicado. Por defecto ```dev```.
* **platform**: Plataforma de destino. Por defecto ```desktop```.
* **target-src**: Carpeta donde se generarán los compilados. Por defecto ```build```.

### Resultado
Como resultado de la compilación tendremos:
* **index.html**: Se copia tal cual el de la carpeta ```src``` y se le añaden las dependencias.
* **app.js**: La aplicación compilada.
* **vendors.js**: Todos los módulos de la carpeta ````node_modules``` separados de la aplicación inicial.
* **styles.css**: Los css de la aplicación.
* **assets**: Se copiarán tal cual los archivos de la carpeta ```src```. Aquí se pondrán recursos como imágenes.

Si se quiere que el css se incluya en ```app.js``` debe sustituirse esta línea:

```javascript
isProduction ? MiniCssExtractPlugin.loader : 'style-loader'
```

por esta:

```javascript
'style-loader'
```

Si no queremos separar las dependencias de la aplicación principal y tenerla en un único ```.js```, debemos eliminar las siguientes lineas:

```javascript
splitChunks: {
	cacheGroups: {
		vendors: {
			test: /[\\/]node_modules[\\/]/,
			name: 'vendors',
			enforce: true,
			chunks: 'all'
		}
	}
}
```


## CLI propia
Este proyecto trae un pequeño script para hacer tareas comunes y facilitar la integración continua:

```
$ node cli <command> [options]
```

### Comando 'build'
Hará todos los pasos para generar la aplicación lista para distribuir. Sus parametros son:

* **platform** (alias ```-p```): Plataforma pasada a la CLI de Webpack. Pueden ser varias y se construirá un ejecutable para cada una. Por defecto ```desktop```.
* **mode** (alias ```-m```): Modo pasado a la CLI de Webpack. Por defecto ```production```.
* **environment** (alias ```-m```): Entorno pasado a la CLI de Webpack. Pueden ser varios y se construirá un ejecutable para cada uno. Por defecto ```prod```.
* **arch** (alias ```-a```): Arquitectura para la cual construir el ejecutable. Puede ser múltiple, pero es posible que para un SO concreto no pueda construirse para esa arquitectura. Los valores posibles son: ```x64```, ```ia32```, ```armv7l```, ```arm64```. Por defecto se construirá para todas.
* **system** (alias ```-s```): Sistema operativo de destino. Sus posibles valores son: ```win32```, ```linux```, ```darwin```. Por defecto se construirá para todos.

### Comando 'clean'
Los compilados y ejecutables finales se almacenarán en ````build``` y ```dist``` respectivamente. Con este comando eliminamos los archivos generados en la construcción en dichas carpetas. No recibe parámetros.

### Comando 'install'
Deja el repositorio en local listo para desarrollar (aunque hay que hacer ```npm install``` antes).

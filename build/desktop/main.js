const { app, BrowserWindow } = require('electron')

let win

function createWindow() {

	// Create the browser window.
	win = new BrowserWindow({
		width: 600,
		height: 600,
		autoHideMenuBar: true,
		show: false,
		backgroundColor: '#ffffff'
	})

	win.loadURL(`${__dirname}/build/src/index.html`)

	// Event when the window is closed.
	win.on('closed', function () {
		win = null
	})

	win.once('ready-to-show', () => {
		win.maximize()
		win.show()
	})
}

// Create window on electron intialization
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {

	// On macOS specific close process
	if (process.platform !== 'darwin')
		app.quit()
})

app.on('activate', function () {
	// macOS specific close process
	if (win === null)
		createWindow()
})

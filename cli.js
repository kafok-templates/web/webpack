const commandLineArgs = require('command-line-args')
const child = require('child_process')
const fs = require('fs')


// Arguments -------------------------------------------

const mainCommand = commandLineArgs([{ name: 'command', defaultOption: true }], { stopAtFirstUnknown: true })
const argv = mainCommand._unknown || []

let options = []
options['build'] = [
	{ name: 'platform', alias: 'p', multiple: true, defaultValue: ['desktop'] },
	{ name: 'mode', alias: 'm', type: String, defaultValue: 'production' },
	{ name: 'environment', alias: 'e', type: String, defaultValue: 'prod' },
	{ name: 'arch', alias: 'a', multiple: true, type: String, defaultValue: ['x64', 'ia32', 'armv7l', 'arm64'] },
	{ name: 'system', alias: 's', multiple: true, type: String, defaultValue: ['win32', 'linux', 'darwin'] }
]

options = options[mainCommand.command] ? commandLineArgs(options[mainCommand.command], { argv }) : {}


// Globals -------------------------------------------

let cwd = '.'
const _package = JSON.parse(fs.readFileSync('package.json', 'utf8'))

const reset = '\x1b[0m'
const blue = '\x1b[34m'
const green = '\x1b[32m'
const yellow = '\x1b[33m'
const red = '\x1b[31m'
const bold = '\x1b[1m'


// Class Pipeline ------------------------------------

class Pipeline {

	constructor(platform, arch, system, commands, texts) {
		this.platform = platform
		this.arch = arch
		this.system = system
		this.commands = commands
		this.texts = texts
		this.phase = 1
		this.iter = 0

		this.longest = 0
		for (let t of this.texts)
			if (t != null && t.length > this.longest)
				this.longest = t.length
	}

	run() {
		let time = new Date().getTime()

		if (this.iter == 0) {
			process.stdout.write(reset + green + `\nStarting building ${_package.name}-${_package.version}` + reset + '\n\n')
			process.stdout.write(reset + bold + `Mode: ${reset + yellow}${options.mode}` + reset + '\n')
			process.stdout.write(reset + bold + `Environment: ${reset + yellow}${options.environment}` + reset + '\n')
			process.stdout.write(reset + bold + `Platforms target: ${reset + yellow}${this.platform}` + reset + '\n')
			process.stdout.write(reset + bold + `Architecture: ${reset + yellow}${this.arch}` + reset + '\n')
			process.stdout.write(reset + bold + `OS: ${reset + yellow}${this.system}` + reset + '\n\n\n')
		}

		if (this.iter < this.commands.length) {

			if (this.texts[this.iter] != null)
				process.stdout.write(`${reset + bold + blue}${this.phase}${reset}) ${this.padding(this.texts[this.iter])}`)

			if (typeof this.commands[this.iter] === 'string') {
				let code = 0
				try {
					child.execSync(this.commands[this.iter], { cwd, stdio: ['ignore', 'ignore', 'pipe'] })
				} catch (error) {
					this.error = error.stderr
					code = error.status
				}

				this.done(code, time)
			} else {
				try {
					this.commands[this.iter]()
					this.done(0, time)
				} catch (code) {
					this.done(typeof code === Number ? code : -1, time)
				}
			}

		} else {
			process.stdout.write(reset + green + '\n\nBUILDED SUCCESSFULLY!!' + reset + '\n')
			process.stdout.write(reset)
		}

	}

	padding(text) {
		let res = ''
		for (let i = 0; i < this.longest - text.length; i++)
			res += ' '

		return text + ':' + res + '\t'
	}

	done(code, time) {
		if (code == 0) {
			if (this.texts[this.iter] != null) {
				let sec = Math.round((new Date().getTime() - time) / 10) / 100
				process.stdout.write(reset + green + 'Done' + reset + ' in ' + yellow + sec + 's\n')

				this.phase++
			}

			this.iter++
			this.run()
		} else {
			process.stderr.write(reset + red + `Failed with code ${code}` + reset + '\n\n')
			if (this.error)
				process.stderr.write(reset + red + this.error + reset + '\n\n')
			process.stderr.write(reset + red + 'BUILD FAILED!!' + reset + '\n')
		}
	}
}



// Utils function -------------------------------------------

const phasePreparing = data => {
	if (data.platform == 'desktop') {
		let res = {}
		res.dependencies = _package.dependencies

		fs.writeFileSync('./build/desktop/package.json', JSON.stringify(res, 4), 'utf-8')
	}
}

const cd = path => {
	return () => cwd = path
}

const clean = all => {
	let deleteFolderRecursive = path => {
		if (fs.existsSync(path)) {
			fs.readdirSync(path).forEach((file, index) => {
				let curPath = path + '/' + file
				if (fs.lstatSync(curPath).isDirectory()) {
					deleteFolderRecursive(curPath)
				} else {
					fs.unlinkSync(curPath)
				}
			})
			fs.rmdirSync(path)
		}
	}

	let deleteFileIfExists = path => {
		if (fs.existsSync(path))
			fs.unlinkSync(path)
	}

	deleteFolderRecursive('./build/desktop/src')
	deleteFileIfExists('./build/desktop/package.json')
	deleteFileIfExists('./build/desktop/package-lock.json')
	deleteFolderRecursive('./build/desktop/node_modules')
	if(all)
		deleteFolderRecursive('./dist')
}

const install = () => {
	let scanFolderRecursive = (path, acum) => {
		if (fs.existsSync(path)) {
			fs.readdirSync(path).forEach((file, index) => {
				let curPath = path + '/' + file

				if (fs.lstatSync(curPath).isDirectory()) {
					scanFolderRecursive(curPath, acum + file + '/')
				} else if (file.endsWith('.d.ts')) {

					if (acum == '') {
						const folder = './node_modules/@types/' + file.replace('.d.ts', '')
						fs.mkdirSync(folder, { recursive: true })

						fs.copyFileSync(curPath, folder + '/' + 'index.d.ts')
					} else {
						const folder = './node_modules/@types/' + acum.replace('.d.ts', '')
						fs.mkdirSync(folder, { recursive: true })

						fs.copyFileSync(curPath, folder + file)
					}
				}
			})
		}
	}

	scanFolderRecursive('./@types', '')
}


// Main -------------------------------------------

switch (mainCommand.command) {

	case 'build':
		clean(true)
		for (let system of options.system) {
			for (let platform of options.platform) {
				for (let arch of options.arch) {
					let data = { platform }
					let pipeline = new Pipeline(platform, arch, system,
						[
							`webpack --mode=${options.mode} --environment=${options.environment} --platform=${platform} --target-src="build/${platform}/src" --display-error-details`,
							() => phasePreparing(data),
							cd('./build/desktop/'),
							'npm install',
							`electron-packager . ${_package.name}-${_package.version} --platform=${system} --arch=${arch} --out="${__dirname + '/dist'}" --asar=true --overwrite=true --prune=true`,
							() => clean(false)
						],
						[
							'Compiling web sources',
							'Preparing building project',
							null,
							'Intalling dependencies',
							`Building application for platform '${platform}'`,
							null
						]
					)
					pipeline.run()
				}
			}
		}
		break

	case 'clean':
		clean(true)
		break

	case 'install':
		install()
		break

}

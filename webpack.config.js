const path = require('path')
const commandLineArgs = require('command-line-args')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const TSLintPlugin = require('tslint-webpack-plugin')


let argv = commandLineArgs([{ name: 'mode', type: String, defaultValue: 'development' },
							{ name: 'environment', type: String, defaultValue: 'dev' },
							{ name: 'platform', type: String, defaultValue: 'desktop' },
							{ name: 'target-src', type: String, defaultValue: 'build' }],
							{ partial: true })



// Extension by enviroments ---------------------------------------

let extensions = ['tsx', 'ts', 'js', 'scss']
let calcResolves = () => {
	let environment = argv.environment
	let platform = argv.platform

	let res = []
	for (let ext of extensions) {
		res.push('.' + platform + '.' + environment + '.' + ext)
		res.push('.' + environment + '.' + ext)
		res.push('.' + platform + '.' + ext)
		res.push('.' + ext)
	}

	return res
}



// Webpack config -----------------------------------------------

module.exports = () => {

	let isProduction = argv.mode == 'production'

	let config = {
		mode: isProduction ? 'production' : 'development',
		resolve: {
			extensions: calcResolves(),
			modules: [path.resolve(__dirname, 'src'), 'node_modules']
		},
		context: __dirname,
		entry: {
			app: ['./src/index.ts']
		},
		output: {
			path: path.join(__dirname, argv['target-src']),
			filename: '[name].js',
			publicPath: ''
		},
		devServer: {
			host: '0.0.0.0',
			port: 8080,
			inline: true
		},
		module: {
			rules: [
				{
					test: /\.scss$/,
					exclude: /node_modules/,
					use: [
						isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
						'css-loader',
						'sass-loader'
					]
				},
				{
					test: /\.tsx?$/,
					use: 'ts-loader',
					exclude: /node_modules/
				}
			]
		},
		plugins: [
			new CleanWebpackPlugin(),
			new HTMLWebpackPlugin({
				filename: 'index.html',
				template: './src/index.html',
			}),

			new CopyWebpackPlugin([
				{ from: 'src/assets', to: 'assets' }
			]),

			new MiniCssExtractPlugin()
		],
		optimization: {
			minimizer: [],
			splitChunks: {
				cacheGroups: {
					vendors: {
						test: /[\\/]node_modules[\\/]/,
						name: 'vendors',
						enforce: true,
						chunks: 'all'
					}
				}
			}
		}
	}

	if(isProduction) {
		config.optimization.minimizer.push(new TerserPlugin())
		config.optimization.minimizer.push(new OptimizeCSSAssetsPlugin())

		config.plugins.push(new TSLintPlugin({
			files: ['./src/**/*.ts', './src/**/*.tsx']
		}))
	}

	return config;
}
